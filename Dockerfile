FROM docker.io/debian:buster

LABEL version=0.1
LABEL name=dev_gui
ENV DEBIAN_FRONTEND noninteractive

RUN set -eux; \
	apt-get update; \
	apt-get -yy --no-install-recommends install \
                xorg \
                dbus-x11 \
                x11-xserver-utils \
                xfce4 \
                xfce4-goodies \
                tigervnc-standalone-server \
                xrdp \
                wget \
                vim \
                ca-certificates \
	; \
        apt-get clean; \
	rm -rf /var/lib/apt/lists/*

# Install gitkraken
# Somehow gitkraken depends on libasound but doesn't 
# state it as a depedency
RUN wget https://release.gitkraken.com/linux/gitkraken-amd64.deb; \
    apt update; \
    apt -yy --no-install-recommends install ./gitkraken-amd64.deb; \
    apt -yy --no-install-recommends install libasound2; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*; \
    rm gitkraken-amd64.deb

# xrdp configuration 
RUN useradd -ms /bin/bash rdpuser; \
    sed -E -i 's/^(TerminalServerUsers.*)/;;\1/g' /etc/xrdp/sesman.ini; \
    sed -E -i 's/^(TerminalServerAdmins.*)/;;\1/g' /etc/xrdp/sesman.ini; \
    sed -i '/\[Xorg\]/,/\[Xvnc\]/c\[Xvnc\]' /etc/xrdp/xrdp.ini; \
    sed -i '/\[vnc-any\]/,$d' /etc/xrdp/xrdp.ini


# Enable snakeoil TLS
RUN adduser xrdp ssl-cert

EXPOSE 3389

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD tail -F /var/log/xrdp-sesman.log
