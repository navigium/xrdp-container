#!/bin/bash
set -x
if [ -f "$HOST_PASSWD" ] && [ -f "$HOST_SHADOW" ]
then
  for USER in $USERS
  do
    grep "$USER" /etc/passwd;
    RC=$?

    if [ $RC -ne 0 ]
    then
      [ -n "$USER" ] && grep "$USER" $HOST_PASSWD >> /etc/passwd
      [ -n "$USER" ] && grep "$USER" $HOST_SHADOW >> /etc/shadow
    fi
  done
fi

service dbus start;
service xrdp start;

exec "$@"
